TEMPLATE = subdirs

SUBDIRS = \
        app \
        lib-hello

lib-hello.subdir = src/lib-hello
app.subdir = src/app

app.depends = lib-hello
