#ifndef HELLO_SHARED_LIB_H
#define HELLO_SHARED_LIB_H

#include "hello_shared_lib_global.h"

class HELLOSHAREDLIB_EXPORT hello_shared_lib
{
public:
    hello_shared_lib();

    quint32 compute_sum(quint32 a, quint32 b);
};

#endif // HELLO_SHARED_LIB_H
