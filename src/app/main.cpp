#include <QCoreApplication>

#include <hello_shared_lib.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    qDebug() << compute_sum(23, 45);
    return a.exec();
}
